function RSA(publicKey, privateKey) {
  var encryptObject = new JSEncrypt();
  var decryptObject = new JSEncrypt();

  encryptObject.setKey(publicKey);
  decryptObject.setKey(privateKey);

  return function (data, encrypt) { //encrypt true 加密
    return encrypt ? encryptObject.encrypt(data) : decryptObject.decrypt(data);
  }
}