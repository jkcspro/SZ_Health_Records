(function($) {
	$.getServiceUrl={
		getUrl:function(urlName){
			var api = "/api/gzd";
			var obj={
				gethosList:"/yygh/getHospitalList",//获取医院列表
				getOrderRecords : "/yygh/getOrderRecords", //获取预约记录
				getFirstList:"/yygh/getNormDeptTopInfo",//获取一级科室
				getSecList:"/yygh/getDeptListTwo",//获取二级科室
				getNumSource:"/yygh/getNumSource"//获取号源

			}
			if(urlName)
				return api+obj[urlName];
			else
				$.QuickAlert.alertFail({
                        content: "未获取到接口地址！"
                    });
		}
	}
})(jQuery);