var judgeAddUrl = {
    
    init:function () {
        this.type = 'wechat';
        this.openid = sessionStorage.getItem('jkcsUserInfo')?JSON.parse(sessionStorage.getItem('jkcsUserInfo')).openid:'';
        this.jzCard = JSON.parse(sessionStorage.getItem('jzCard') || sessionStorage.getItem('defaultCardInfo'));
        if(this.jzCard){ this.lastStr = encodeURIComponent('&q={"WDMedicalCard":"'+this.jzCard.cardNo+'","WDOpenId":"'+this.openid+'"}&Sign='+hex_md5('{"WDMedicalCard":"'+this.jzCard.cardNo+'","WDOpenId":"'+this.openid+'"},QMWJW').toUpperCase());}
        var code = this.getCode();
        var htmlName = this.judge();
        var htmlUrl = this.redirect(code,this.type,htmlName);
        this.userInfo = JSON.parse(sessionStorage.getItem('defaultCardInfo'));
        if(JSON.parse(sessionStorage.getItem('familyList'))){
            this.userInfo = JSON.parse(sessionStorage.getItem('familyList'));
        }
        if(sessionStorage.getItem('backRight')){
            sessionStorage.removeItem('backRight');
            location.replace("/html/gzd/navigation/index.html");
        }
        if(htmlUrl && sessionStorage.getItem('jgdm') && sessionStorage.getItem('jgdm')==42505626100){
            sessionStorage.removeItem('history');
            sessionStorage.setItem('backRight',1);
            // this.getUserid(this.userInfo.xm,this.userInfo.sfzh,this.userInfo.sj);
            
            //最新注释修改
            // this.getUserid(JSON.parse(sessionStorage.getItem('jkcsUserInfo')).jmjbxxid);
            $.QuickAlert.alertFail({
                content: "功能优化中！"
            });
            return false
        }
        else if(htmlUrl && sessionStorage.getItem('jgdm') && sessionStorage.getItem('jgdm')!=42505626100){
            sessionStorage.removeItem('history');
            sessionStorage.setItem('backRight',1);
            this.fnUrlReplace(htmlUrl);
            sessionStorage.removeItem('jgdm');
            sessionStorage.removeItem('jgmc');
        }else{
           
        }
    },
    getCode:function(){//医院机构代码获取
        var code = $.QuickUrlUtils.getRequest('hosCode') || sessionStorage.getItem('jgdm');
        return code;
    },
    getUserid:function(jmjbxxid){
        // userName,userCard,userPhone
        var that = this;
        $.QuickRemote.AjaxJson({
            funcName: "/api/gzd/only/getSignature",
            params:{
                // userName:userName,
                // userPhone:userPhone, 
                // userCard:userCard
                jmjbxxid:jmjbxxid
            },
            callback: function(result) {
                that.tid =result.tid;
                that.str = 'appkey=pdqwdbmfwpt&idcard='+that.userInfo.sfzh+'&mobile='+that.userInfo.sj+'&patientName='+that.userInfo.xm+'&tid='+result.tid
                that.signature = result.signature;
                that.fnUrlReplace(that.redirect(that.getCode(),that.type,that.judge()));
            }
        });
    },
    judge:function(){//版本判断
        var urls = location.href;
        var htmlName='';
        htmlName = urls.substring(urls.lastIndexOf('/')+1,urls.indexOf('.html'));
        // var u = navigator.userAgent;
        // if (/MicroMessenger/.test(u)) {
        //     this.type = 'wechat';
        // } else if (/AlipayClient/.test(u)) {
        //     this.type = 'alipay';
        // } else {
        //     this.type = 'other';
        // }
        return htmlName;
    },
    fnUrlReplace:function (eleLink) {
        if (!eleLink) {
            return;
        }
        var div = document.createElement("div");
        div.style.cssText="position:fixed;left:0;top:0;width:100vw;height:100vh;background-color:#fff;z-index:999;";
        div.innerText="医院跳转中。。。";
        document.body.appendChild(div);
        if(sessionStorage.getItem('jgdm')==42505626100){
            var href = eleLink.split('&redirect_uri=');
            var redirect = href[1];
            var type = redirect.split('&response_type=');
            var str = href[0]+'&redirect_uri='+encodeURIComponent(type[0])+'&response_type='+type[1];
        }else{
            var str = eleLink;
        }
        console.log(str)
        location.replace(str);
    },
    redirect:function (code,type,htmlName) {//外链地址添加
        var hospitalList = {
            "42501615500":{
                name:'公利医院',
                alipay:{
                    // https://pdzfb.pdwjk.gov.cn
                    'hos-index':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e&nextUrl=/sk_area/funlist?hid=928d98d9-e98a-464b-9e1c-f7da4f86530a',//医院首页
                    // 'date-regist':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/guahao/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//预约挂号
                    'date-regist':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/guahao/index',//预约挂号
                    // wdyy:'https://fwcs.linkingcloud.cn/Account/LoginIn?id=a6bada99-9016-4c3d-9d72-a7444afc4d8a&nextUrl=/yuyue/index',//我的预约
                    // 'outpatient-payment-list':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/bill/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//门诊账单记录
                    'outpatient-payment-list':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/bill/index',//门诊账单记录
                    // 'register-history-list':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/yuyue/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//挂号记录
                    'register-history-list':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/yuyue/index',//挂号记录
                    'waiting-list':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/houzhen/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',
                },
                wechat:{
                    'hos-index':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e&nextUrl=/sk_area/funlist?hid=928d98d9-e98a-464b-9e1c-f7da4f86530a',//医院首页
                    // 'date-regist':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/guahao/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//预约挂号
                    'date-regist':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/guahao/index',//预约挂号
                    // wdyy:'https://fwcs.linkingcloud.cn/Account/LoginIn?id=a6bada99-9016-4c3d-9d72-a7444afc4d8a&nextUrl=/yuyue/index',//我的预约
                    // 'zdjl':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/bill/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//门诊账单记录
                    'zdjl':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/bill/index',//门诊账单记录
                    // 'register-history-list':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/yuyue/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',//挂号记录
                    'register-history-list':'https://wjwzfb.pudong.gov.cn/Account/LoginIn?id=928d98d9-e98a-464b-9e1c-f7da4f86530a&nextUrl=/yuyue/index',//挂号记录
                    'waiting-list':'https://wjwzfb.pudong.gov.cn/account/LoginvIn/41c132d8-c0e2-4ba5-84a8-de2f56d9cd7e?nextUrl=/houzhen/index&hospitalID=928d98d9-e98a-464b-9e1c-f7da4f86530a',
                }
            },
            "42505128400":{
                name:'第七人民医院',
                wechat:{
                    'hos-index':'https://h5.shqmxx.com/QrResource.aspx?P=kDmaK72_smPQDmTGqzYzm//q4MBFFvKajU9ZbfV4do2vYXCpgF_RcspRD30v4TCwhZDZzEhUIfn88tYRmVcRsDnJ8_rK7oveWhsi3TDsTq4=',//医院首页
                    'date-regist':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf94f6a24c7ef4554&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d31%26Pri%3dQM.CSFP.31.DIS.RegistrationLayout-RegistrationNotice%26UiServiceId%3dgh_103a0dd726d0%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hmOTRmNmEyNGM3ZWY0NTU0JTJjOTU4OTYxOTU1MmU1MWJkZWM5MmNlNzk2MDIwOTMwOTIlMmMw#wechat_redirect',//当日挂号
                    'register-history-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf94f6a24c7ef4554&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d31%26Pri%3dQM.CSFP.31.DIS.RegistrationLayout-MyRegistrationInfos%26UiServiceId%3dgh_103a0dd726d0%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hmOTRmNmEyNGM3ZWY0NTU0JTJjOTU4OTYxOTU1MmU1MWJkZWM5MmNlNzk2MDIwOTMwOTIlMmMw#wechat_redirect',//挂号单查询
                    'outpatient-payment-list':'https://h5.shqmxx.com/QrResource.aspx?P=cvbCX8QhN5VRMp8FPKDbbqiOEFx5O9HXHSNO0ZG0s7gUYbSrJpc2OK1jWOeo42okywqWAjW7u1bpDOEHD7pISWFCRZ5GGFkN/uFEOFUw9/s=',//门诊账单记录
                    'waiting-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf94f6a24c7ef4554&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d31%26Pri%3dQM.CSFP.31.DIS.QueueLayout-ShowMyQueue%26UiServiceId%3dgh_103a0dd726d0%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hmOTRmNmEyNGM3ZWY0NTU0JTJjOTU4OTYxOTU1MmU1MWJkZWM5MmNlNzk2MDIwOTMwOTIlMmMw#wechat_redirect'//候诊查询
                }
            },
            "42505626100":{
                name:'浦东医院',
                wechat:{
                    'hos-index':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1754b980d256f229&redirect_uri=https://weixin.ngarihealth.com/weixin/page.html?'+this.str+'&appid=wx1754b980d256f229&signature='+this.signature+'&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect',//医院首页
                    'date-regist':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1754b980d256f229&redirect_uri=https://weixin.ngarihealth.com/weixin/page.html?'+this.str+'&appid=wx1754b980d256f229&signature='+this.signature+'&module=appointHomePage&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect',//当日挂号
                    'hospital-detail':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1754b980d256f229&redirect_uri=https://weixin.ngarihealth.com/weixin/page.html?'+this.str+'&appid=wx1754b980d256f229&signature='+this.signature+'&module=InHspSearch&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect',//住院首页
                    'register-history-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx1754b980d256f229&redirect_uri=https://weixin.ngarihealth.com/weixin/page.html?'+this.str+'&appid=wx1754b980d256f229&signature='+this.signature+'&module=userAppointIndex&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect',//挂号单查询
                }
            },
            "42491010400":{
                name:'周浦医院',
                wechat:{
                    'hos-index':'https://h5.shqmxx.com/QrResource.aspx?P=zBw7YecThvp60RafIKZwCLItXLevcg015ANFmwVGgowNAAD1AAzRkA29WmlmJ6hWcJDvnuC2e/Dmt6uNTO4UMtxnK8gmaDPl/2EcVzkGQXI=',//医院首页
                    'date-regist':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxc682e6144e0754d4&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d170%26Pri%3dQM.CSFP.170.DIS.Registration-ChoiceType%26UiServiceId%3dgh_b5660772b09b%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hjNjgyZTYxNDRlMDc1NGQ0JTJjYmMxZjBiZjk3MmU2ZGViMzM2NjBjZWRlNTFjOWRmN2YlMmMw#wechat_redirect',//当日挂号
                    'register-history-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxc682e6144e0754d4&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d170%26Pri%3dQM.CSFP.170.DIS.Registration-ShowOwn%26UiServiceId%3dgh_b5660772b09b%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hjNjgyZTYxNDRlMDc1NGQ0JTJjYmMxZjBiZjk3MmU2ZGViMzM2NjBjZWRlNTFjOWRmN2YlMmMw#wechat_redirect',//挂号单查询
                    'outpatient-payment-list':'https://h5.shqmxx.com/QrResource.aspx?P=SheuAt3Qo6R_m5OVYihWicJo3fCglt8qlk/5OhSNunjPdtt6F/9JyTN9HQqwA6XZzoT7pByLjA224oSDg4/3WoErLRqXV8uPi61xvWOfciA=',//门诊费用单
                    'waiting-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxc682e6144e0754d4&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d170%26Pri%3dQM.CSFP.170.DIS.WaitingQueue-QueueOwn%26UiServiceId%3dgh_b5660772b09b%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hjNjgyZTYxNDRlMDc1NGQ0JTJjYmMxZjBiZjk3MmU2ZGViMzM2NjBjZWRlNTFjOWRmN2YlMmMw#wechat_redirect'//候诊查询
                }
            },
            '42509273X00':{
                name:'人民医院',
                wechat:{
                    'hos-index':'https://h5.shqmxx.com/QrResource.aspx?P=KIhknwFg8NI2hLH1CoW9Owys6R2W54AENQJBmyvg/BCCgmNlRPheowRcYboM3zFMgU28QEShcWLRv9KU_YScrV/iuifdl3pfrC13X7H0Jzw=',//医院首页
                    'date-regist':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa223413b646dc65b&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d52%26Pri%3dQM.CSFP.52.DIS.Registration-ChoiceType%26UiServiceId%3dgh_22a2d9c84b9c%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hhMjIzNDEzYjY0NmRjNjViJTJjMjgyMTBjOGIyZGYzODdiMzIyNjM3ZTk5YzFmYzZhNDElMmMw#wechat_redirect',//当日挂号
                    'register-history-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa223413b646dc65b&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d52%26Pri%3dQM.CSFP.52.DIS.Registration-ShowOwn%26UiServiceId%3dgh_22a2d9c84b9c%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3hhMjIzNDEzYjY0NmRjNjViJTJjMjgyMTBjOGIyZGYzODdiMzIyNjM3ZTk5YzFmYzZhNDElMmMw#wechat_redirect',//挂号单查询
                    'waiting-list':'https://h5.shqmxx.com/QrResource.aspx?P=WLF8xVUe/oLFRZLxTRwDGBw1yMFxH0neKLuj5V0nUrxnStOFRCSATWca2RV4dyS55SBtyMQfmeTZdQ4ALk4DQUX7C3QSsoCVekQWwISwX/Q=',
                    'hospital-detail':'https://h5.shqmxx.com/QrResource.aspx?P=Dxr3h0y6VE/yqbFu_RoC3G6TN6RX3RFTjNGnUoH8vOuQ27MjTH0xR09HQkwQfW39zVR4CA4/0_7LD3XqL/vOBFqw7m3MeAugN8i27byvkrQ=',//预交金充值
                }
            },
            "42502710200":{
                name:'浦南医院',
                wechat:{
                    'hos-index':'https://h5.shqmxx.com/QrResource.aspx?P=dnXvkqJZxye6BA3EKgblLNIvRrsS0vic8MUcV0ZQR9yxMt12IdZd0DZW4hw18uj4za5YFrXsVTPTBseomBaJN0gv/KRS4zL26JZO4T/L_FI='+this.lastStr,//医院首页
                    'date-regist':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx784ac2305735e6b7&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d115%26Pri%3dQM.CSFP.115.DIS.RegistrationLayout-ShowRegistrationType%26UiServiceId%3dgh_ea559ff46326%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3g3ODRhYzIzMDU3MzVlNmI3JTJjOWUzNGYxODhjZDBlZDI5OTBhZjNjMjljNDRiNmY1ZGQlMmMw#wechat_redirect',//当日挂号
                    'register-history-list':'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx784ac2305735e6b7&redirect_uri=https%3a%2f%2fh5.shqmxx.com%2fResource.aspx%3fCustomerId%3d115%26Pri%3dQM.CSFP.115.DIS.RegistrationLayout-MyRegistrationInfos%26UiServiceId%3dgh_ea559ff46326%26UItype%3dwx%26IsMenu%3d1%26Version%3d1.1'+this.lastStr+'&response_type=code&scope=snsapi_base&state=d3g3ODRhYzIzMDU3MzVlNmI3JTJjOWUzNGYxODhjZDBlZDI5OTBhZjNjMjljNDRiNmY1ZGQlMmMw#wechat_redirect',//挂号单查询
                    'waiting-list':'https://h5.shqmxx.com/QrResource.aspx?P=R5GLR3Yv/VONVPn1kDEpL6qzeaq9WQ7okir9B1JnigSm1GpIPR3Id5df9pEdbjP1vzV/N1YtlIefGSKayxgRlBINbRQ6eLFFre8_Tdoq5PU='+this.lastStr,
                    'outpatient-payment-list':'https://h5.shqmxx.com/QrResource.aspx?P=0/ZK8fblxSf0BbVDvaOFovfvtecZxulr2HQz99f04P334lkeHMwY8Dexci9goQlrbHwUhojVbAvYXKgFJhCbzAYzJEMVfN_xiNvXtoiSfLA='+this.lastStr,
                }
            }
        }
        // if(hospitalList[code]){
        if(code){
            if(hospitalList[code][type]){
                return hospitalList[code][type][htmlName] || '';
            }else{
                return '';
            }
        }else{
            return '';
        }
    }
}
$(function () {
    judgeAddUrl.init();
})
/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }

/*
 * Perform a simple self-test to see if the VM is working
 */
function md5_vm_test()
{
    return hex_md5("abc") == "900150983cd24fb0d6963f7d28e17f72";
}

/*
 * Calculate the MD5 of an array of little-endian words, and a bit length
 */
function core_md5(x, len)
{
    /* append padding */
    x[len >> 5] |= 0x80 << ((len) % 32);
    x[(((len + 64) >>> 9) << 4) + 14] = len;

    var a =  1732584193;
    var b = -271733879;
    var c = -1732584194;
    var d =  271733878;

    for(var i = 0; i < x.length; i += 16)
    {
        var olda = a;
        var oldb = b;
        var oldc = c;
        var oldd = d;

        a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
        d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
        c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
        b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
        a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
        d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
        c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
        b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
        a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
        d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
        c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
        b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
        a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
        d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
        c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
        b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

        a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
        d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
        c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
        b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
        a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
        d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
        c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
        b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
        a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
        d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
        c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
        b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
        a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
        d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
        c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
        b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

        a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
        d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
        c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
        b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
        a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
        d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
        c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
        b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
        a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
        d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
        c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
        b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
        a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
        d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
        c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
        b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

        a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
        d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
        c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
        b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
        a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
        d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
        c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
        b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
        a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
        d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
        c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
        b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
        a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
        d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
        c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
        b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

        a = safe_add(a, olda);
        b = safe_add(b, oldb);
        c = safe_add(c, oldc);
        d = safe_add(d, oldd);
    }
    return Array(a, b, c, d);

}

/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
    return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
    return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
    return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
    return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
    return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}

/*
 * Calculate the HMAC-MD5, of a key and some data
 */
function core_hmac_md5(key, data)
{
    var bkey = str2binl(key);
    if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);

    var ipad = Array(16), opad = Array(16);
    for(var i = 0; i < 16; i++)
    {
        ipad[i] = bkey[i] ^ 0x36363636;
        opad[i] = bkey[i] ^ 0x5C5C5C5C;
    }

    var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
    return core_md5(opad.concat(hash), 512 + 128);
}

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
    var lsw = (x & 0xFFFF) + (y & 0xFFFF);
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xFFFF);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
    return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str)
{
    var bin = Array();
    var mask = (1 << chrsz) - 1;
    for(var i = 0; i < str.length * chrsz; i += chrsz)
        bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
    return bin;
}

/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin)
{
    var str = "";
    var mask = (1 << chrsz) - 1;
    for(var i = 0; i < bin.length * 32; i += chrsz)
        str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
    return str;
}

/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray)
{
    var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
    var str = "";
    for(var i = 0; i < binarray.length * 4; i++)
    {
        str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
    }
    return str;
}

/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray)
{
    var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var str = "";
    for(var i = 0; i < binarray.length * 4; i += 3)
    {
        var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16)
            | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
            |  ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
        for(var j = 0; j < 4; j++)
        {
            if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
            else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
        }
    }
    return str;
}
