(function ($) {
  $.getUrl = {
    pageUrl: function (pageName, urlName) {
      var pageUrls = {
        // 大众端
        "beforLogin": {
          "login": "/szjkda/html/gzd/auth/login.html",
          "forgetPwd": "/szjkda/html/gzd/auth/forgetPwd.html",
          "register": "/szjkda/html/gzd/auth/register.html",
          "read": "/szjkda/html/gzd/common/read.html"
        },
        "index": {
          "index": "/szjkda/szjkda/html/gzd/navigation/index.html",
          "myOrder": "/szjkda/html/gzd/myOrder/yygh.html",
          "mySignService": "/szjkda/html/gzd/signService/wdqy.html",
          "onliePay": "/szjkda/html/gzd/HospitalPayment/HospPayment/findBills.html",
          "treatmentPay": "/szjkda/html/gzd/HospitalPayment/outpatient-payment/outpatient-payment.html",
          'zyjf': '/szjkda/html/gzd/hospitalRecords/hospitalRecords.html',
          'hzcx': '/szjkda/html/gzd/waiting/index.html',
          // "elePaper":"/szjkda/html/gzd/HospitalService/electPrescription/searchPres.html",
          "searchReport": "/szjkda/html/gzd/HospitalService/inspention/search-report.html",
          "treatment": "/szjkda/html/gzd/navigation/treatment.html",
          "register": "/szjkda/html/gzd/register/register.html",
          "newsRemind": "/szjkda/html/gzd/news/newsRemind.html",
          "signChat": "/szjkda/html/gzd/signChat/mySignService.html",
          "consult": "/szjkda/html/gzd/consult/zxzx.html",
          "onlineConsult": "/szjkda/html/gzd/consult/onlineConsultAsk.html",
          //"yyjz":"/szjkda/html/gzd/yyjz/list.html"
          "yyjz": "/szjkda/html/gzd/yyjz/main.html"
        },
        "waiting": { //候诊查询模块
          'index': '/szjkda/html/gzd/waiting/index.html',
          'list': '/szjkda/html/gzd/waiting/waiting-list.html'
        },
        "search": {
          "searchInput": "/szjkda/html/gzd/search/searchInput.html",
          "searchResult": "/szjkda/html/gzd/search/searchResult.html",
          "searchHospital": "/szjkda/html/gzd/search/searchList/hospitalList.html",
          "searchDoctor": "/szjkda/html/gzd/search/searchList/doctorList.html",
          "searchJb": "/szjkda/html/gzd/search/searchList/jbList.html",
          "searchKs": "/szjkda/html/gzd/search/searchList/ksList.html",
          "searchYp": "/szjkda/html/gzd/search/searchList/ypList.html"
        },
        "mySignService": {
          "wdqy": "/szjkda/html/gzd/signService/wdqy.html",
          "qyls": "/szjkda/html/gzd/signService/qyls.html",
          "tdjj": "/szjkda/html/gzd/signService/tdjj.html",
          "xzfwtc": "/szjkda/html/gzd/signService/xzfwtc.html",
          "yqyqr": "/szjkda/html/gzd/signService/yqyqr.html",
          "ysjj": "/szjkda/html/gzd/signService/ysjj.html",
          "newsRemind": "/szjkda/html/gzd/news/newsRemind.html",
          "hasContract": "/szjkda/html/gzd/signService/hasContract.html"
        },
        "myOrder": {
          "hosDetail": '/szjkda/html/gzd/myOrder/hosDetail.html',
          "ksDetail": '/szjkda/html/gzd/myOrder/ksDetail.html',
          "docDetail": '/szjkda/html/gzd/myOrder/ysDetail.html',
          "orderInfo": "/szjkda/html/gzd/myOrder/yyInfo.html",
          "orderDetail": "/szjkda/html/gzd/myOrder/yyDetail.html",
          "wdyy": '/szjkda/html/gzd/myOrder/wdyy.html',
          "searchYy": "/szjkda/html/common/myOrder/searchYy.html",
          "yyDetail": "/szjkda/html/gzd/myOrder/yyDetailWithoutPay.html",
          "hos-index": "/szjkda/html/gzd/myOrder/hos-index.html"
        },
        "family": {
          "familyList": '/szjkda/html/gzd/familyList/familyList.html',
          "familyAdd": "/szjkda/html/gzd/familyList/familyAdd.html",
          "familyInfo": "/szjkda/html/gzd/familyList/familyInfo.html",
          "selfCenter": "selfCenter",
          "familyPatientCard": "/szjkda/html/gzd/familyList/familyPatientCard.html",
          "familyUpdate": "/szjkda/html/gzd/familyList/familyUpdate.html",
          'realName': '/szjkda/html/gzd/familyList/realName.html',
          "selHostpital": "/szjkda/html/gzd/familyList/selHospital.html",
        },
        "onlineConsult": {
          "wdzx": '/szjkda/html/onlineConsult/wdzx.html',
          "onlineConsultAsk": "/szjkda/html/gzd/consult/onlineConsultAsk.html",

          "consultDetail": "/szjkda/html/gzd/consult/consultDetail.html",
          "questList": "/szjkda/html/gzd/consult/questList.html",
          "questDetailList": "/szjkda/html/gzd/consult/questionDetailList.html"
        },
        "selfCenter": {
          "wdyy": '/szjkda/html/gzd/myOrder/wdyy.html',
          "wdqy": '/szjkda/html/gzd/signService/qyls.html',
          "familyList": '/szjkda/html/gzd/familyList/familyList.html',
          "wdjkk": '/szjkda/html/gzd/jkk/list.html',
          "jfjl": '/szjkda/html/gzd/HospitalPayment/outpatient-payment/outpatient-payment-history-list.html',
          "register-history-list": '/szjkda/html/gzd/register/register-history-list.html',
          // "zyjl":"/szjkda/html/gzd/hospitalRecords/hospitalRecords.html",
          "wdyx": '/szjkda/html/healthapp/medicineKit/medicineKit.html',
          "sz": '/szjkda/html/gzd/navigation/userSetting.html',
          "personal": "/szjkda/html/gzd/navigation/personal.html",
          "consultList": "/szjkda/html/gzd/consult/questionList.html",
          "healthy-server": "/szjkda/html/gzd/navigation/healthy-server.html",
          "wdsc": "/szjkda/html/gzd/consoleDisplayPage/jkzx/wdsc-jkzx.html",
        },
        "measurement": {
          "suger_mobile": "/szjkda/html/gzd/measurement/suger_mobile.html",
          "records_mobile": "/szjkda/html/gzd/measurement/records_mobile.html",
          "BMI_mobile": "/szjkda/html/gzd/measurement/BMI_mobile.html",
          "oxygen_mobile": "/szjkda/html/gzd/measurement/oxygen_mobile.html",
          "WHR_mobile": "/szjkda/html/gzd/measurement/WHR_mobile.html",
          "pressure_mobile": "/szjkda/html/gzd/measurement/pressure_mobile.html",
          "selfTest_mobile": "/szjkda/html/gzd/measurement/selfTest_mobile.html",
          "temperature": "/szjkda/html/gzd/measurement/temperature.html",
          "uricAcid": "/szjkda/html/gzd/measurement/uricAcid.html",
          "xindian": "/szjkda/html/gzd/measurement/records_mobile.html?cllx=08"
        },
        "healthInfo": {
          //"healthInfoIndex":"/szjkda/html/gzd/consoleDisplayPage/jkzx/jkzx-sy.html"
        },
        "healthRecord": {
          "healthRecordIndex": "/szjkda/html/gzd/jkda/index.html"
        },
        "diseaseTest": {
          "partList": "/szjkda/html/gzd/diseaseTest/partList.html",
          "diseaseList": "/szjkda/html/gzd/diseaseTest/diseaseList.html",
          "diseaseDetail": "/szjkda/html/gzd/diseaseTest/diseaseDetail.html",
          "diseaseTestIndex": "/szjkda/html/gzd/diseaseTest/diseaseTest.html"
        },
        "disease-expalin": {
          "hospital-list": "/szjkda/html/gzd/disease-explain/hospital-list.html",
          "disease-list": "/szjkda/html/gzd/disease-explain/disease-list.html",
          "zndz": "/szjkda/html/gzd/disease-explain/zndz.html",
          "drugs-list": "/szjkda/html/gzd/disease-explain/drugs-list.html",
          "drugs-detail": "/szjkda/html/gzd/disease-explain/drugs-detail.html",
        },
        "userSetting": {
          "userChangePass": "/szjkda/html/gzd/auth/userChangePass.html",
          "userAdvice": "/szjkda/html/gzd/navigation/selfCenter/userAdvice.html",
          "userAboutUs": "/szjkda/html/gzd/navigation/selfCenter/userAboutUs.html",
          "usersAgreement": "/szjkda/html/gzd/navigation/selfCenter/usersAgreement.html",
          "person": "/szjkda/html/gzd/navigation/personal.html"
        },
        "common": {
          "findHospital": "/szjkda/html/gzd/common/findHospital.html",
          "selectePerson": "/szjkda/html/gzd/common/selectePerson.html",
          "hospitalListing": "/szjkda/html/gzd/hospitalRecords/hospitalListing.html",
          "map": "/szjkda/html/gzd/common/map.html"
        },
        "jkda": {
          "main": "/szjkda/html/gzd/jkda/index.html",
          "mzjl": "/szjkda/html/gzd/jkda/jzjl/mzjl.html",
          "zyjl": "/szjkda/html/gzd/jkda/jzjl/zyjl.html",
          "cyxj": "/szjkda/html/gzd/jkda/jzjl/cyxj.html",
          "mzDetail": "/szjkda/html/gzd/jkda/jzjl/mzDetail.html",
          "zyDetail": "/szjkda/html/gzd/jkda/jzjl/zyDetail.html",
          "dzcfDetail": "/szjkda/html/gzd/jkda/jzjl/dzcf/dzcfDetail.html",
          "sfjl": "/szjkda/html/gzd/jkda/sfjl/sfjl.html",
          "jcjl": "/szjkda/html/gzd/jkda/jyjc/jcjl.html",
          "jyjl": "/szjkda/html/gzd/jkda/jyjc/jyjl.html",
          "jcbg": "/szjkda/html/gzd/jkda/jyjc/jcbg.html",
          "jybg": "/szjkda/html/gzd/jkda/jyjc/jybg.html",
          "ssjl": "/szjkda/html/gzd/jkda/ssjl/ssjl.html",
          "tjbg": "/szjkda/html/gzd/jkda/tjbg/tjbg.html",
          "mbbg": "/szjkda/html/gzd/jkda/mbda/mbda.html",
          "yyxq": "/szjkda/html/gzd/jkda/yyxx/yyxq.html",
          "yxjp": "/szjkda/html/gzd/jkda/jyjc/exam-search.html", //暂时
          "yxjpList": "/szjkda/html/gzd/jkda/jyjc/exam-list.html",
          "yxjpInfo": "/szjkda/html/gzd/jkda/jyjc/exam-info.html",
          "tjbgDetail": "/szjkda/html/gzd/jkda/tjbgDetail.html", //暂时
          "tjbgList": "/szjkda/html/gzd/jkda/tjbgList.html", //暂时
          "sfDetail": '/szjkda/html/gzd/jkda/sfjl/sfDetail.html'
        },
        "onlinePay": {
          "myBills": "/szjkda/html/gzd/HospitalPayment/HospPayment/myBill.html"
        },
        "treatmentPay": {
          "index": "/szjkda/html/gzd/HospitalPayment/treatmentPay/czzyjf.html",
          "zyjrResult": "/szjkda/html/gzd/HospitalPayment/treatmentPay/zyjfResult.html",
          "prePay": "/szjkda/html/gzd/HospitalPayment/treatmentPay/yjjcz.html",
          "prePayList": "/szjkda/html/gzd/HospitalPayment/treatmentPay/czzyjf.html",
          'billList': '/szjkda/html/gzd/hospitalRecords/date-list.html'
        },
        "searchReport": {
          "searchReport": "/szjkda/html/gzd/HospitalService/inspention/search-report.html",
          "reportList": "/szjkda/html/gzd/HospitalService/inspention/reportList.html",
          "jyreportDetail": "/szjkda/html/gzd/HospitalService/inspention/jybgxq.html",
          "jcreportDetail": "/szjkda/html/gzd/jkda/HospitalService/inspention/jcbgxq.html"
        },
        "electPrescription": {
          "presListing": "/szjkda/html/gzd/HospitalService/electPrescription/presListing.html",
        },
        "hospitalRecords": {
          "hospitalListing": "/szjkda/html/gzd/hospitalRecords/hospitalListing.html",
        },
        "mzDetail": {
          "dzcf": "/szjkda/html/gzd/jkda/jzjl/dzcf/dzcfList.html",

        },
        "signService": {
          "qyqr": "/szjkda/html/common/signService/doc_qyqr.html",
          "patientHomepage": "/szjkda/html/ysd/zyhz/hzzym.html",
          "collectResidents": "/szjkda/html/ysd/qyjm/collectResidents.html",
          "qyjm": "/szjkda/html/ysd/qyjm/qyjm_index.html",
          "tdjj": "/szjkda/html/common/signService/tdjj.html",
        },
        "jkzx": {
          "index": "/szjkda/html/gzd/consoleDisplayPage/jkzx/jkzx-sy.html",
          "detail": "/szjkda/html/gzd/consoleDisplayPage/bmgg/jkzx-content.html",
          "wdsc": "/szjkda/html/gzd/consoleDisplayPage/jkzx/wdsc-jkzx.html" //暂时
        },
        "treatment": {
          "yygh": "/szjkda/html/gzd/myOrder/hosDetail.html",
          "xsjf": "/szjkda/html/gzd/HospitalPayment/HospPayment/findBills.html"
        },
        "news": {
          "newsRemind": "/szjkda/html/gzd/news/newsRemind.html",
        },
        "familyDoc": {
          "hasContract": "/szjkda/html/gzd/familyDoc/hasContract.html",
          "noContract": "/szjkda/html/gzd/familyDoc/noContract.html",
          "noPrescription": "/szjkda/html/gzd/familyDoc/noPrescription.html",
          "hasPrescription": "/szjkda/html/gzd/familyDoc/hasPrescription.html",
          "logistics": "/szjkda/html/gzd/familyDoc/logistics.html",
          "prescriptionDetail": "/szjkda/html/gzd/familyDoc/prescriptionDetail.html",
        },
        "estimate": {
          "jzpj": "/szjkda/html/gzd/estimate/jzpj.html",
          "jzpjxq": "/szjkda/html/gzd/estimate/jzpjxq.html"
        },
        "qyfw": {
          "zxyqy": "/szjkda/html/gzd/qyfw/qygrxx.html",
          "qyfw": "/szjkda/html/gzd/qyfw/index.html",
          "wait": "/szjkda/html/gzd/qyfw/wait.html",
          "zxqy": "/szjkda/html/gzd/qyfw/signOnline.html"
        },
        "yscf": {
          "index": "/szjkda/html/gzd/qyfw/yscf/index.html",
          "detail": "/szjkda/html/gzd/qyfw/yscf/detail.html",
          "detail_wl": "/szjkda/html/gzd/qyfw/yscf/detail_wl.html",
          'referral-order': '/szjkda/html/gzd/qyfw/yscf/referral-order.html',
          'referral-order-detail': '/szjkda/html/gzd/qyfw/yscf/referral-order-detail.html'
        },
        "zzdd": {
          "index": "/szjkda/html/gzd/qyfw/yscf/referral-order.html"
        },
        "realName": {
          'realNameNav': '/szjkda/html/gzd/auth/realname-nav.html',
          "realNameByPhone": "/szjkda/html/gzd/auth/realNameByPhone.html",
          "realNameByCard": "/szjkda/html/gzd/auth/realNameByCard.html",
          'realNameByWechat': "/szjkda/html/gzd/auth/realNameByWechat.html"
        },
        "yyjz": {
          "list": "/szjkda/html/gzd/yyjz/list.html",
          "detail": "/szjkda/html/gzd/yyjz/detail.html",
          "jzjf": "/szjkda/html/gzd/yyjz/jzjf.html",
        },
        "private": {
          "index": "/szjkda/html/gzd/jkda/privacy/index.html",
          "detail": "/szjkda/html/gzd/jkda/privacy/details.html"
        }
        // 'outpatientPayment':{暂时
        // 	'search':'/szjkda/html/gzd/HospitalPayment/outpatient-payment/outpatient-payment.html',
        // 	'list':'/szjkda/html/gzd/HospitalPayment/outpatient-payment/outpatient-payment-list.html',
        //    'detail':'/szjkda/html/gzd/HospitalPayment/outpatient-payment/outpatient-payment-detail.html',
        // }
      };
      if (urlName) {
        var urls = pageUrls[pageName];
        if (urls) {
          return urls[urlName];
        }
        return null;
      } else {
        return pageUrls[pageName];
      }
    }
  }
})(jQuery);