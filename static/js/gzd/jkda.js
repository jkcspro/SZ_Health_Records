

(function() {
    // 健康档案
    $.getMyorderUrl = {
        getpageUrl : function(urlName) {
            var jkda = {
                // 就诊记录
                "queryJzjl" : "queryJzjl",
                // 电子处方详情
                "getPrescriptionDetail" : "getPrescriptionDetail",
                // 电子处方概要
                "getPrescriptionRecord" : "getPrescriptionRecord",
                // 查询就诊记录
                "queryJzjl" : "queryJzjl",
                // 查询门诊检查报告
                "queryMzJc" : "queryMzJc",
                // 查询门诊检查报告详情
                "queryMzJcbgxq" : "queryMzJcbgxq",
                // 查询门诊检验报告
                "queryMzJy" : "queryMzJy",
                // 查询门诊检验报告详情
                "queryMzJybgxq" : "queryMzJybgxq",
                // 查询住院检查报告
                "queryZyJc" : "queryZyJc",
                // 查询住院检查报告详情
                "queryZyJcbgxq" : "queryZyJcbgxq",
                // 查询住院检验报告
                "queryZyJy" : "queryZyJy",
                // 查询住院检验报告详情
                "queryZyJybgxq" : "queryZyJybgxq",
                // 查询住院史
                "queryZys" : "queryZys",
                // 查询住院史详情
                "queryZysxq" :"queryDetailZyYsbg",
                //查询出院小结
                "getReport":"getPrescriptionZycyxj"
            };
            return "/api/gzd/jkda/" + jkda[urlName];
        }
    }







})(jQuery)