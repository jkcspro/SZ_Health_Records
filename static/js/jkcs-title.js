(function($) {
    $.JkcsTile = {
        getTitle: function(key,params){
            var option = {
                "default":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "index":{
                    "type": "update",
                    "color": "0dbbff",
                    "left": {
                        "type": "image",
                        "imgName": "erweima",
                        "callBackMethod": "indexLeft"
                    },
                    // "right": {
                    //     "type": "image",
                    //     "imgName": "xinxi",
                    //     "callBackMethod": "indexRight"
                    // },
                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索科室、医院、医生",
                        "callBackMethod": "indexTitle",
                        /*  "searchMethods": {
                         "editingdidbegin" : "indexTitle"
                         }*/
                    }
                },
                "sc":{
                    "type": "update",
                    "color": "f5f7fc",
                    // "right": {
                    //     "type": "image",
                    //     "imgName": params["scImg"],
                    //     "callBackMethod": "scFunction"
                    // },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                 "zxxq":{
                    "type": "update",
                    "color": "f5f7fc",
                    "right": {
                        "type": "title",
                        "title":"结束",
                        "callBackMethod": "btnEnd"
                    },
                    
                },
                "qyls":{
                    "type": "update",
                    "color": "f5f7fc",
                    "right": {
                        "type": "image",
                        "imgName": "qyls",
                        "callBackMethod": "toQyls"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "wdqy":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "image",
                        "imgName": "qyls",
                        "callBackMethod": "toQyls"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "jz":{
                    "type": "update",
                    "color": "0dbbff",
                    // "left": {
                    //     "type": "image",
                    //     "imgName": params["scImg"],
                    //     "callBackMethod": "scFunction"
                    // },
                    // "right": {
                    //     "type": "image",
                    //     "imgName": "xinxi",
                    //     "callBackMethod": "indexRight"
                    // },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "health":{
                    "type": "update",
                    "color": "0dbbff",
                    "left": {
                        "type": "image",
                        "imgName": "erweima",
                        "callBackMethod": "indexLeft"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "jzPop":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "image",
                        "imgName": params["scImg"],
                        "callBackMethod": "scFunction"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "yygh":{
                    "type": "update",
                    "color": "0dbbff",
                    // "right": {
                    //     "type": "image",
                    //     "imgName": "search",
                    //     "callBackMethod": "search"
                    // },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "ksDetail":{
                    "type": "update",
                    "color": "0dbbff",
                    // "right": {
                    //     "type": "image",
                    //     "imgName": "ks_sx",
                    //     "callBackMethod": "sxShowOrHide"
                    // },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "familyList":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "image",
                        "imgName": "icon_add",
                        "callBackMethod": "goAddInfo"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "familyAdd":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "title",
                        "title": "完成",
                        "callBackMethod": "saveInfo"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "familyUpdate":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "title",
                        "title": "完成",
                        "callBackMethod": "saveInfo"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "healthTest":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "title",
                        "title": "提交",
                        "callBackMethod": "submit"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "qyjmIndex":{
                    "type": "update",
                    "color": "0dbbff",

                    // "right": {
                    //     "type": "image",
                    //     "imgName": "plus",
                    //     "callBackMethod": "toXzhz"
                    // },
                    "titleView": {
                        "type": "search",
                        "placeholder": "快速搜索",
                        "searchMethods": {
                            "editingdidbegin" : "queryByCondition",
                            "editingdidend" : "queryByCondition",
                            "editingchanged" : "queryByCondition"
                        }
                    }
                },
                "yyjj":{
                    
                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索医院、医生",
                        "searchMethods": {
                            "editingdidbegin" : "searchDoc",
                            "editingdidend" : "searchDoc",
                            "editingchanged" : "searchDoc"
                        }
                    }
                },
                "ysgrsy":{
                    "type": "update",
                    "color": "0dbbff",

                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索医院、医生",
                        "callBackMethod":"toJqss"
                    }
                },
                "yyss":{
                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索医院、医生",
                        "searchMethods": {
                            "editingdidbegin" : "searchDoc",
                            "editingdidend" : "searchDoc",
                            "editingchanged" : "searchDoc"
                        }
                    },
                    "right": {
                        "type": "title",
                        "title": "筛选",
                        "callBackMethod": "toMzyysx"
                    },
                    
                },
                "yyjc":{
                    "type": "update",
                    "color": "0dbbff",
                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索",
                        "searchMethods": {
                            "editingdidbegin" : "initView",
                            "editingdidend" : "initView",
                            "editingchanged" : "initView"
                        }
                    },
                    "right": {
                        "type": "title",
                        "title": "检查",
                        "callBackMethod": "toYsJcyysx"
                    },

                },
                "yycw":{
                    "type": "update",
                    "color": "0dbbff",

                    "titleView": {
                        "type": "search",
                        "placeholder": "搜索医院、科室、医生",
                        "searchMethods": {
                            "editingdidbegin" : "initView",
                            "editingdidend" : "initView",
                            "editingchanged" : "initView"
                        }
                    },
                    "right": {
                        "type": "title",
                        "title": "床位",
                        "callBackMethod": "toYycwsx"
                    },

                },
                "healthCard_register":{
                    "type": "update",
                    "color": "0dbbff",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "healthCard_modifyInfo":{
                    "type": "update",
                    "color": "0dbbff",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "findBills":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "czzyjf":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "billsPay":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "selectePerson":{
                    "type": "update",
                    "color": "0dbbff",
                    "right": {
                        "type": "image",
                        "imgName": "icon_add",
                        "callBackMethod": "goAddInfo"
                    },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "yjjcz":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "czzyjf":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                "yjjList":{
                    "type": "update",
                    "color": "#f5f7fc",
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },
                 "wdyyList":{
                    "type": "update",
                    "color": "0dbbff",
                    // "right": {
                    //     "type": "title",
                    //     "title": "个人中心",
                    //     "callBackMethod": "saveInfo"
                    // },
                    "titleView": {
                        "type":"title",
                        "title": params["title"]
                    }
                },



            };
            return option[key];
        }
    }
})(jQuery);