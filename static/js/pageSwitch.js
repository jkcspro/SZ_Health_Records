(function($) {
	$.pageSwitch={
		Safety:function(sUrl){
			return (/^(http(s)?:\/\/)?[\w\-.]+\.(mhealth.jynstudio|wjwgzh.pudong.gov)\.(com|cn)($|\/|\\)/i).test(sUrl)? true : false; 
		},
		clickToUrl:function(divContant,pageName,urlparmas){
			//需要跳转的div 需要加上class = runDiv modele='' 在pageUrl.json中配置url地址。 
				var page = $.getUrl.pageUrl(pageName);
				
				 $(divContant).on("click", ".runDiv", function() {
				 	if(!divContant || !pageName){
						$.QuickAlert.alertFail({
	                        content: "当前页面跳转参数配置错误!"
	                    });
	                    return false;
					}
					var moduleName = $(this).attr("module");
				 	var url = page[moduleName];
				 	if(!url){
						$.QuickAlert.alertFail({
	                        content: "该模块暂未开放!敬请期待！"
	                    });
	                    return false;
					}
					urlparmas = urlparmas?"?"+urlparmas:"";
					url = url+urlparmas;
					$.QuickUrlUtils.redirectJSBridge({'url':url});
				 	
				 });
		},
		goToUrl:function(pageName,urlName,urlparmas,option,data){
			/*跳转方式。option可以传空，默认annimate为push {
                    "animate":options.animate==undefined?"push":options.animate,
                    "params":data!=undefined?data:{},
                }*/
                if(!pageName || !urlName){
					$.QuickAlert.alertFail({
                        content: "跳转参数配置错误!"
                    });
                    return false;
				}
               if(!option) option ={};
			
					var pageUrl = $.getUrl.pageUrl(pageName,urlName);
					if(!pageUrl){
						$.QuickAlert.alertFail({
	                        content: "该模块暂未开放!敬请期待！"
	                    });
	                    return false;
					}
					urlparmas = urlparmas?"?"+urlparmas:"";
				 	option.url = pageUrl+urlparmas;
				 	if(option && option.animate && option.animate=='pop' && !option.noRefresh){
				 		option.refreshUrl = option.url;
					}
					
					$.QuickUrlUtils.redirectJSBridge(option,data);
					
				 	

		}
	}
})(jQuery);
