(function($) {
	/**网页端基于layer
	 * 兼容安卓，苹果
	 * 依赖quickVersion
	 */
	$.QuickAlert = {
		alertNormal : function(content, config) {
			var configFinal = $.extend({}, defaultConfig, config, {
				content : content
			});
			var index = parent.layer.open({
				title : [
					'<div class="layer-header"><i class="icon-layer-info"></i>' + configFinal["title"] + '</div>',
				],
				closeBtn : 1,
				shadeClose : true,
				fix : false,
				content : configFinal["content"],
				skin : "new",
				icon : 1,
				btn : [ '确认' ],
				end : function() {
					parent.layer.close(index);
					var callBack = configFinal["callback"];
					if (callBack) callBack();
				}
			});
		},
		alertFail : function(config) {
			var configFinal = $.extend({}, defaultConfig, config);
			parent.layer.open({
				title : [
					'<div class="layer-header"><i class="icon-layer-error"></i>' + configFinal["title"] + '</div>',
				],
				closeBtn : 1,
				shadeClose : true,
				fix : false,
				content : configFinal["content"],
				skin : "new",
				icon : 5,
				btn : [ '确认' ],
				end : function(index) {
					parent.layer.close(index);
					var callback = configFinal["callback"];
					if (callback) callback();
				}
			});
		},
		alertConfirm : function(config) {
			var btns = {btn : [ '确认', '取消' ]};
			// var configFinal = $.extend({}, btns, defaultConfig, config);
			var cfg = {
				title : ['<div class="layer-header"><i class="icon-layer-question"></i>' + defaultConfig["title"] + '</div>'],
				closeBtn : 2,
				shadeClose : true,
				fix : false,
				content : defaultConfig["content"],
				skin : "new",
				icon : 3
				,yes : function(index) {
					parent.layer.close(index);
					var callback = configFinal["callback"];
					if (callback) callback();
				}
			};
			// $.each(configFinal, function(key, value){
			// 	if(key != "btn" && key.indexOf("btn") != -1)
			// 	{
			// 		cfg[key] = value;
			// 	}
			// 	else if(key == "btn")
			// 	{
			// 		cfg[key] = value;
			// 	}
			// })
            var configFinal = $.extend({}, btns, defaultConfig, cfg, config);
			parent.layer.open(configFinal);
			/*parent.layer.open({
				title : [
					'<div class="layer-header"><i class="icon-layer-question"></i>' + configFinal["title"] + '</div>',
				],
				closeBtn : 2,
				shadeClose : true,
				fix : false,
				content : configFinal["content"],
				skin : "new",
				icon : 3,
				btn : [ '确认', '取消' ],
				yes : function(index) {
					parent.layer.close(index);
					var callBack = config["callBack"];
					if (callBack) callBack();
				}
				
			});*/
		},
		alertLoading : function() {
				return parent.layer.open({
					type : 2,
					content : '加载中',
					shadeClose : false,
					fixed : true
				});
		},
		alertIFrame : function(config) {
			if ($.QuickVersion["isWeb"]) {
				var configFinal = $.extend({}, iframeConfig, config);
				parent.layer.open({
					type : 2,
					title : configFinal["title"],
					shadeClose : configFinal["shadeClose"],
					shade : 0.8,
					area : [configFinal["width"], configFinal["height"]],
					content : configFinal["content"]
					,end: configFinal["end"]
					,cancel: function(index){
						var func = configFinal["cancel"];
                        if(func){
                        	return func(index);
						}else{
                        	return true;
						}
					}
				});
			} else {
				/*layer mobile不支持弹出iframe层*/
				alert("浏览器不支持");
			}
		}
	}

	var iframeConfig = {
		title : "温馨提示",
		width : "580",
		height : "500",
		shadeClose : true
	}

	var defaultConfig = {
		title : "温馨提示",
		callBack : null,
		params : null
	};
})(jQuery);