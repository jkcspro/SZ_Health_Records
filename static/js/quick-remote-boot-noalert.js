/**前后台交互工具类*/
(function ($) {
  jQuery.support.cors = true;
  $(document).ajaxStart($.noop);
  $(document).ajaxSuccess($.noop);
  $(document).ajaxError($.noop);


  var result_code = {
    SUCCESS: "SUCCESS",
    ERROR: "ERROR",
    EXCEPTION: "EXCEPTION"
  };
  var handler = {
    SUCCESS: function (ops) {
      var result = ops['result'];
      var config = ops['config'];
      var additionParams = config['additionParams'];
      var callback = config['callback'];
      if (config.showbox.hasClass("quick_remote_loading_showbox"))
        config.showbox.remove();
      else
        config.showbox.html($(this).data("showbox_info"));
      $(this).removeData("quick_remote_ajaxJson_config");
      if (additionParams != null) {
        callback.apply(this, [result['data'], additionParams]);
      } else {
        callback.apply(this, [result['data']]);
      }
    },
    ERROR: function (ops) {
      var result = ops['result'];
      var config = ops['config'];
      var errorback = config['errorback'];
      if (config.showbox.hasClass("quick_remote_loading_showbox"))
        config.showbox.html("加载失败：" + result.message);
      else
        config.showbox.html($(this).data("showbox_info"));
      $(this).removeData("quick_remote_ajaxJson_config");

      errorback.apply(this, [result]);

    },
    EXCEPTION: function (ops) {
      var result = ops['result'];
      var config = ops['config'];
      var exceptionback = config['exceptionback'];
      if (config.showbox.hasClass("quick_remote_loading_showbox"))
        config.showbox.html("系统异常：" + result.message);
      else
        config.showbox.html($(this).data("showbox_info"));
      $(this).removeData("quick_remote_ajaxJson_config");

      exceptionback.apply(this, [result]);
    }
  };
  var default_ajaxConfig = {
    type: "POST" // 使用get方法访问后台
      ,
    dataType: "json" // 返回json格式的数据
      ,
    data: {} // 要发送的数据
    ,
    headers: {}
  };
  var default_config = {
    suffix: ".service" //服务后缀
      ,
    isEncode: true //是否对参数进行编码
      ,
    callback: $.noop //成功回调
      ,
    errorback: $.noop //错误回调
      ,
    exceptionback: $.noop //异常回调
      ,
    additionParams: {} //传入回调的额外参数
  };
  var default_message = {
    loading: "正在加载",
    loadError: undefined,
    loadException: undefined
  };
  /**
   * json格式的ajax交互
   * @param config 方法自定义参数，也可以是请求的url
   * @param ajaxConfig $.ajax的参数
   */
  var ajaxJson = function ($this, config, ajaxConfig) {
    var cfg = $.extend({}, default_config, config);
    var option = $.extend({
      success: function (result) {
        try {
          var code = result['code'];
          var code = result['code'];
          if (code == 0) {
            config.callback(result);
          }
        } catch (err) {
          if (!$.QuickVersion.isIE()) {
            console.log(err);
          }
        }
      }
    }, default_ajaxConfig, config, ajaxConfig);
    //对参数进行编码
    if (cfg.isEncode) {
      var data = option.data;
      for (var key in data) {
        var value = data[key];
        data[key] = encodeURI(value);
      }
      option.data = data;
    }
    // //给url增加后缀，如果是以后缀结尾则忽略
    // if (!option.url.endsWith(cfg.suffix)) {
    //     option.url = option.url + cfg.suffix;
    // }
    //发送请求
    $.ajax(option);
  };

  $.fn.extend({
    ajaxJson: function (config, ajaxConfig, messageConfig) {
      $(this).data("quick_remote_ajaxJson_config", {
        config: config,
        ajaxConfig: ajaxConfig,
        messageConfig: messageConfig
      }); //提供重试，记录重试的参数
      var message = $.extend({}, default_message, messageConfig);
      var showbox = $(this).find(message.showbox);
      //处理加载提示框
      if (showbox.length <= 0) {
        // 1、如果没配置提示框，则动态添加，并在加载成功后删除，加载失败后在提示框显示失败信息；
        var width = $(this).width();
        var heignth = $(this).height();
        showbox = $("<div>")
          .addClass("quick_remote_loading_showbox")
          .width(width)
          .height(heignth)
          .attr("style", "");
        $(this).find("*").hide();
        $(this).append(showbox);
      } else {
        // 2、如果配置了提示框，则在加载时显示加载信息，加载失败后显示原信息；
        var showbox_info = showbox.html();
        $(this).data("showbox_info", showbox_info);
      }
      config.showbox = showbox;
      showbox.html(message.loading);
      ajaxJson($(this), config, ajaxConfig);
    }
  });
})(jQuery);