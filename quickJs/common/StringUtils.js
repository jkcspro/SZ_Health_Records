function StringUtils() {
}
/** 字符串是否为空 */
StringUtils.isEmpty = function(str) {
	if (str == null || $.trim(str) == "" || str.length == 0 || str == 'undefined')
		return true
	else
		return false;
}
/** 获取字符串长度 */
StringUtils.getRealLength = function(str) {
	var l = 0;
	var a = str.split("");
	for (var i = 0; i < a.length; i++) {
		if (a[i].charCodeAt(0) < 299) {
			l++;
		} else {
			l += 2;
		}
	}
	return l;
}

/** 全数字字符串 */
StringUtils.isNumStr = function(str) {
	var reg = new RegExp("^(0|[1-9][0-9]*)$");
	return reg.test(str);
}

/**是否已特定的字符串开头*/
StringUtils.startWith = function(sourceStr, withStr) {
	if(withStr==null||withStr=="")
	  return false;
	if(sourceStr.substr(0,withStr.length)==withStr)
	  return true;
	else
	  return false;
}

