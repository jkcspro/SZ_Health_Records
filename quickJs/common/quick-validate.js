/**验证插件*/
(function ($) {

    /*验证默认配置*/
    var validateDefaultConfig = {
        /*验证错误默认提示方法*/
        tipsFunc: defaultTipsFunc
        /*是否全部校验*/
        , validateAll: false
    }

    /*弹出提示框*/
    function defaultTipsFunc(validateFailMsg, index) {
        var _this = $(this);
        $.QuickAlert.alertFail({
            content: validateFailMsg
            , callback: function () {
                _this.focus();
            }
        });
    }

    /**
     * 根据 validateProxy 属性 链式调用
     */
    $.fn.QuickValidate = function (options) {
        var config = $.extend({}, validateDefaultConfig, options);
        var isValid = true;
        var tipsFunc = config["tipsFunc"];
        var validateAll = config["validateAll"];
        this.each(function (index) {
            var config = $.QuickParser.getConfig(this);
            /**值*/
            var value = $(this).QuickField("getData");
            /**验证失败提示信息*/
            var validateFailMsg = config["validateFailMsg"];
            var validateProxy = config["validateProxy"];
            if (!StringUtils.isEmpty(validateProxy)) {
                var proxyAry = validateProxy.split(" ");
                var flag = true;
                var _this = $(this);
                $.each(proxyAry, function (index, name) {
                    var func = $.fn.QuickValidate.method[name];
                    var result = "";
                    if (func) {
                        result = func(value, config);
                    }
                    else {
                        func = eval(name);
                        result = func.apply(_this, [value, config]);
                        if (!StringUtils.isEmpty(result)) {
                            flag = false;
                        }
                    }
                    if (!StringUtils.isEmpty(result)) {
                        flag = false;
                        if (StringUtils.isEmpty(validateFailMsg)) {
                            validateFailMsg = result;
                        }
                    }
                    return flag;
                })

                if (!flag) {
                    isValid = flag;
                    tipsFunc.apply(_this, [validateFailMsg, index]);
                }
            }
            if (validateAll) {
                return true;
            } else {
                return flag;
            }
        })
        return isValid;
    }

    $.fn.QuickValidate.method = {
        /**字符串非空*/
        notNull: function (value, config) {
            var result = "";
            if (StringUtils.isEmpty(value)) {
                result = config["label"] + "不能为空";
            }
            return result;
        }
        /**长度校验*/
        , length: function (value, config) {
            var result = "";
            var len = StringUtils.getRealLength(value);
            var length = config["length"];
            var ary = length.split("-");
            if (ary.length == 1) {
                if (len > ary[0]) {
                    result = config["label"] + "最大长度为" + ary[0] + "个字符";
                }
            }
            else if (len < ary[0] || len > ary[1]) {
                if (ary[0] == ary[1]) {
                    result = config["label"] + "的长度应为" + ary[0] + "个字符";
                }
                else {
                    result = config["label"] + "的长度应在" + ary[0] + "-" + ary[1] + "之间";
                }
            }
            return result;
        }
        /**字母数字*/
        , charNum: function (value, config) {
            var result = "";
            var reg = /^[0-9a-zA-Z]*$/g;
            if (!reg.test(value)) {
                result = config["label"] + "只能由字母和数字组成";
            }
            return result;
        }
        /**字母数字下划线*/
        , charNumUnderline: function (value, config) {
            var result = "";
            var reg = /^\w+$/g;
            if (!reg.test(value)) {
                result = config["label"] + "只能由字母，数字和下划线组成";
            }
            return result;
        }
        /**数字*/
        , num: function (value, config) {
            var result = "";
            var reg = /^[0-9]*$/g;
            if (!reg.test(value)) {
                result = config["label"] + "只能由数字组成";
            }
            return result;
        }
        , D10: function (value, config) {
            var result = "";
            if (!StringUtils.isEmpty(value)) {
                /*
                 var date = value.stringToDate();
                 if(!date){
                 result = config["label"] + "格式为:YYYY-MM-DD";
                 }
                 */
                var timeReg = new RegExp("^[1-2]\\d{3}-(0?[1-9]||1[0-2])-(0?[1-9]||[1-2][0-9]||3[0-1])$");
                if (!timeReg.test(value)) {
                    result = config["label"] + "格式为:YYYY-MM-DD";
                }
            }
            return result;
        }
        , idCard: function (value, config) {
            var result = "";
            var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if (!reg.test(value)) {
                result = config["label"] + "格式不正确";
            }
            return result;
        }
        , newIdCard: function (value, config) {
            var city = {
                11: "北京",
                12: "天津",
                13: "河北",
                14: "山西",
                15: "内蒙古",
                21: "辽宁",
                22: "吉林",
                23: "黑龙江 ",
                31: "上海",
                32: "江苏",
                33: "浙江",
                34: "安徽",
                35: "福建",
                36: "江西",
                37: "山东",
                41: "河南",
                42: "湖北 ",
                43: "湖南",
                44: "广东",
                45: "广西",
                46: "海南",
                50: "重庆",
                51: "四川",
                52: "贵州",
                53: "云南",
                54: "西藏 ",
                61: "陕西",
                62: "甘肃",
                63: "青海",
                64: "宁夏",
                65: "新疆",
                71: "台湾",
                81: "香港",
                82: "澳门",
                91: "国外 "
            };
            var tip = "";
            var pass = true;

            value = value.toUpperCase();
            if (!value || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)?$/i.test(value)) {
                tip = "格式错误";
                pass = false;
            }

            else if (!city[value.substr(0, 2)]) {
                tip = "地址编码错误";
                pass = false;
            }
            else {
                //18位身份证需要验证最后一位校验位
                if (value.length == 18) {
                    value = value.split('');
                    //∑(ai×Wi)(mod 11)
                    //加权因子
                    var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
                    //校验位
                    var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
                    var sum = 0;
                    var ai = 0;
                    var wi = 0;
                    for (var i = 0; i < 17; i++) {
                        ai = value[i];
                        wi = factor[i];
                        sum += ai * wi;
                    }
                    var last = parity[sum % 11];
                    if (parity[sum % 11] != value[17]) {
                        tip = "校验位错误";
                        pass = false;
                    }
                }
            }
            if (!pass) {
                tip = config["label"] + tip;
            };
            return tip;
        }
    }
})(jQuery);















