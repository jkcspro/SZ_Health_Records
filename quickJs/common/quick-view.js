/**页面视图插件*/
(function($) {
	/**注册页面*/
	$.fn.registerQuickView = function(config)
	{
		$(".quick-btn").QuickBtn();
		$(".quick-form").QuickForm();
		$(".quick-datagrid").QuickDatagrid();
		/**处理按钮事件 兼容动态添加的元素*/
		$(this).on("click", ".quick-btn", function(){
			$(this).QuickBtn("proxyClick");
		});
		
		if(config)
		{
			var endFunc = config["endFunction"];
			if(endFunc)
			{
				endFunc();
			}
		}
	};

	var registerManagerPageDefaultConfig = {
		/*注册页面的ID*/
		container: "body"
		/*自动获取页面缓存：需要使用quick-zydm的后台项目和前台js 具体参看js文件说明*/
		,autoLoadCache: true
		/*除html元素已配置cacheTable之外的cacheTable*/
		,cacheTables: []
		/*自动设置html元素（一般select）选项*/
        ,setCacheTableData: true
		/*添加必填项class*/
		,addNotNullClass: true
		/*回调*/
		,callback: null
		/*是否自动初始化quickfield元素*/
		,autoInitQuickField: true
	}
    /**
	 * 注册编辑页面
	 * 1、识别页面配置的值域缓存
	 * 2、接收页面未配置的值域缓存
	 * 3、是否自动设置原始select下拉框选项
	 * 4、根据label中文匹配 为qiuckField项目的label加上必填*
	 * 5、自动初始化quickField对象
     * @param config
     */
	$.fn.registerManagerPage = function(config, params){
		if(typeof config == "string"){
		}else{
			/*初始化操作*/
            var finalConfig = $.extend({}, registerManagerPageDefaultConfig, config);
            var container = finalConfig["container"];
            var autoLoadCache = finalConfig["autoLoadCache"];
            var cacheTables = finalConfig["cacheTables"];
            var setCacheTableData = finalConfig["setCacheTableData"];
            var addNotNullClass = finalConfig["addNotNullClass"];
            var callback = finalConfig["callback"];
            var autoInitQuickField = finalConfig["autoInitQuickField"];
            /*初始化容器内的quickfield对象*/
            if(autoInitQuickField){
                initQuickField(container);
			}
            /*必填项加上class*/
			if(addNotNullClass){
                addNotNullClassFunc(container);
			}
			if(autoLoadCache){
				$(container).getZydmCache({
					cacheTables: cacheTables
					,setCacheTableData: setCacheTableData
					,callback: function(cacheData){
						if(callback){
                            callback(cacheData);
						}
					}
				})
			}
		}
	}
	/*初始化quickField*/
	function initQuickField(container){
        $(container).find(".form-control").QuickField();
	}
	/*quickField加上必填class*/
	function addNotNullClassFunc(container){
        $(container).find(".form-control").each(function(){
        	$(this).QuickField("addNotNullClass");
        });
	}
})(jQuery);









