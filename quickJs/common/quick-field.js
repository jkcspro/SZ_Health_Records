(function($) {
	var setDataFunction = "setDataFunction";
	var getDataFunction = "getDataFunction";
	$.fn.QuickField = function(options, params){
		if($.type(options) == "string")
		{
			return $.fn.QuickField.method[options](this, params);
		}
		else
		{
			return this.each(function(){
				$.QuickParser.parseConfig(this);
			});
		}
	};
	
	$.fn.QuickField.method = {
		setData: function(_this, value)
		{
			var config = $.QuickParser.getConfig(_this);
			var func = config[setDataFunction];
			if(func)
			{
				func.call(_this, value);
			}
			else
			{
				var type = $(_this)[0].type;
				if(type == 'text' || type == 'textarea' || type == 'select-one' || type=='password')
				{
					$(_this).val(value);
				}
				else if (type == 'radio') {
					var name = $(_this).attr('name');
					$("input[name='" + name + "']").each(function() {
						if (value == $(this).val()) {
							$(this).prop("checked", true);
							$(this).trigger("click");
						}
					})
				}
			}
		}
		,getData: function(_this)
		{
			var config = $.QuickParser.getConfig(_this);
			var func = config[getDataFunction];
			if(func)
			{
				return func.apply(_this);
			}
			else
			{
				var type = $(_this)[0].type;
				if(type == 'text' || type == 'textarea' || type == 'select-one' || type=='password')
				{
					var sss = $(_this).val();
					return sss;
				}
				else if (type == 'radio') {
					var name = $(_this).attr('name');
					return $("input[name='" + name + "']:checked").val();
				}
			}
		}
		,addNotNullClass: function(_this){
            var config = $.QuickParser.getConfig(_this);
            var validateProxy = config["validateProxy"];
            if(!StringUtils.isEmpty(validateProxy) && validateProxy.indexOf("notNull") != -1){
                var labeltext = config["label"];
				var label = $("label:contains(" + labeltext + "):first");
				var span = $("<span class='quick-not-null'>*</span>")
                $(label).before(span);
			}
		}
	};
})(jQuery);























