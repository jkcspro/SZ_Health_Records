/**配置解析器*/
(function ($) {
	$.QuickParser = $.extend($.QuickBase, {
		/**解析 _this 绑定的元素*/
		parseConfig: function(_this, options)
		{
			var jq = $(_this);
			var config = jq.attr(this.quickConfigLabel);
			if(!StringUtils.isEmpty(config))
			{
				config = parseStrToJson(config);
			}
			if($.type(options) == "string")
			{
				options = parseStrToJson(options);
			}
			// var existConfig = $.data(jq, this.quickConfigLabel);
            var existConfig = jq.data(this.quickConfigLabel);
			config = $.extend({}, existConfig, config, options);
			jq.data(this.quickConfigLabel, config);
		}
		,getConfig: function(_this)
		{
            var jq = $(_this);
			var config = jq.data(this.quickConfigLabel);
			if($.isEmptyObject(config) || !$.isPlainObject(config))
			{
				$.QuickParser.parseConfig(jq);
				return jq.data(this.quickConfigLabel);
			}
			return config;
		}
	});
	
	/**字符串解析为json*/
	function parseStrToJson(str)
	{
		var json = {};
		var ary = str.split(",");
		$(ary).each(function(){
			var ary1 = this.split(":");
			/**去掉左右空格*/
			var key = ary1[0].replace(/^\s+|\s+$/g, "");
			var value = ary1[1].replace(/^\s+|\s+$/g, "").replace(/'/g, '');
			if(key.toLowerCase().indexOf("function") != -1)
			{
				value = eval(value);
				if(!$.isFunction(value))
				{
					value = null;
				}
			}
			json[key] = value;
		})
		return json;
	}
})(jQuery);










