/**基于store的核心缓存类*/
var StoreCache = (function(){
	return {
		/**设置缓存*/
		setCache: function(key, value)
		{
			store.set(key, value);
		}
		/**返回并删除缓存*/
		,removeCache: function(key)
		{
			var value = store.get(key);
			store.remove(key);
			return value;
		}
		/**获取缓存*/
		,getCache: function(key)
		{
			var value = store.get(key);
			return value;
		}
	}
})()