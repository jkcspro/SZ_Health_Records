(function ($) {
  // var u = navigator.userAgent.toLowerCase();
  var u = navigator.userAgent,
    app = navigator.appVersion;
  /**
   * 浏览器判断
   */
  $.QuickVersion = {
    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios终端
    android: u.indexOf('android') > -1 || u.indexOf('Linux') > -1, // android终端或uc浏览器
    iPhone: u.indexOf('iphone') != -1, // 是否为iPhone或者QQHD浏览器
    iPad: u.indexOf('ipad') > -1, // 是否iPad
    isWeb: u.indexOf('windows nt') != -1 || (u.indexOf('mac os') != -1 && u.indexOf('ios_appname') == -1), //是否PC浏览器 不完善！！！
    isWeixin: u.indexOf('micromessenger') != -1, //是否微信
    isAndroid: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, // Android
    isIPhone: u.indexOf('iPhone') > -1, // ios
    isIE: function () {
      if (!!window.ActiveXObject || "ActiveXObject" in window)
        return true;
      else
        return false;
    },
    isMobileApp: function (key) {
      return u.indexOf(key) != -1;
    }
  }
})(jQuery);