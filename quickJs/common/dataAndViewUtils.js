/** 数据帮助类 */
(function($) {
	var options;
	var data = {};
	/**记录页面当前输入的值 兼容easyui的datagrid*/
	$.fn.reginsterData = function(options)
	{
		options = options;
		$(".dataField").each(function(){
			var value = getValue(this);
			data = $.extend(data, value);
		});
		$(this).bind('beforeunload',function(){
			if(compare())
			{
				return '您输入的内容尚未保存，确定离开此页面吗？';
			}
		});
	}
	
	function compare()
	{
		var isChange = false;
		$(".dataField").each(function(){
			var dataField = $(this).attr("name");
			if($(this).hasClass("easyui-combobox"))
			{
				dataField = $(this).attr("comboname");
			}
			var newValue = getValue(this);
			newValue = newValue[dataField];
			var oldValue = data[dataField];
			if(!$.isArray(oldValue))
			{
				if(oldValue != newValue)
				{
					isChange = true;
					return false;
				}
			}
			else
			{
				/**数组做最简单的判断 转为字符串比较*/
				if(oldValue.join("") != newValue.join(""))
				{
					isChange = true;
					return false;
				}
			}
		});
		return isChange;
	}
	
	function getValue(inputItem)
	{
		var data = {};
		var dataField = $(inputItem).attr("name");
		var type = $(inputItem)[0].type;
		/**兼容easyui的combobox*/
		if($(inputItem).hasClass("easyui-combobox"))
		{
			var value = $(inputItem).combobox("getValue");
			dataField = $(inputItem).attr("comboname");
			data[dataField] = value;
		}
		else if (type == 'text' || type == 'password' || type=='textarea' || type=='hidden') {
			var value = $(inputItem).val();
			data[dataField] = value;
		} 
		else if (type == 'select-one') 
		{
			var value = $(inputItem).val();
			data[dataField] = value;
			var labelField = $(inputItem).attr("labelField");
			if(!StringUtils.isEmpty(labelField))
			{
				data[labelField] = $(inputItem).find("option:selected").text();
			}
		}
		else if (type == 'radio') 
		{
			var name = $(inputItem).attr('name');
			var value =  $('input[name=' + name + ']:checked').val();
			data[dataField] = StringUtils.isEmpty(value)?'':value;
		}
		/**兼容easyui的datagrid*/
		else if($(inputItem).hasClass("datagrid-f"))
		{
			var dgId = $(inputItem).attr("id");
			var ary = $("#"+dgId).datagrid("getRows");
			ary = $.extend([], ary);
			data[dataField] = ary;
		}
		return data;
	}
})(jQuery);

/**获取form表单值*/
(function($) {
    $.fn.getFormData = function(){
        var params = {};
        var ary = $(this).serializeArray();
        $.each(ary, function(index, item){
            params[item["name"]] = item["value"];
        })
		return params;
	}
})(jQuery);























/************************!!!!!!!!!!!!以下脚本不定时删除!!!!!!!!!!!!!!!!!!!!!!*******************************************/
/**
 * 获取容器数据
 * 
 * @param {}
 *            containerId 容器ID
 * @param {}
 *            className 输入项className
 */
function getContainerDataByClassName(containerId, className, data) 
{
	if (data == null || data == 'undefined') 
	{
		data = {};
	}
	var ary = $("#" + containerId + " ." + className);
	if (ary != null && ary.length != 0) 
	{
		for (var i = 0; i < ary.length; i++)
		{
			var inputItem = ary[i];
			var dataField = $(inputItem).attr("name");
			if(StringUtils.isEmpty(dataField))
			{
				dataField = $(inputItem).attr("dataField");
			}
			var type = $(inputItem)[0].type;
			if (type == 'text' || type == 'password' || type=='textarea' || type=='hidden') {
				var value = $(inputItem).val();
				data[dataField] = value;
			} 
			else if (type == 'select-one') 
			{
				var value = $(inputItem).val();
				data[dataField] = value;
				var labelField = $(inputItem).attr("labelField");
				if(!StringUtils.isEmpty(labelField))
				{
					data[labelField] = $(inputItem).find("option:selected").text();
				}
			} 
			else if (type == 'radio') 
			{
				var name = $(inputItem).attr('name');
				var value =  $('#' + containerId + ' input[name=' + name + ']:checked').val();
				data[dataField] = StringUtils.isEmpty(value)?'':value;
			}
		}
	}
	return data;
}

/**
 * 为指定容器内指定的className控件赋值
 * 
 * @param {}
 *            containerId
 * @param {}
 *            className
 * @param {}
 *            data
 */
function setContainerDataByClassName(containerId, className, data) {
	var ary = $("#" + containerId + " ." + className);
	if (ary != null && ary.length != 0) {
		for (var i = 0; i < ary.length; i++) {
			var inputItem = ary[i];
			var dataField = $(inputItem).attr("name");
			if(StringUtils.isEmpty(dataField))
			{
				dataField = $(inputItem).attr("dataField");
			}
			var value = data[dataField];
			var type = $(inputItem)[0].type;
			/**兼容easyui的combobox*/
			if($(inputItem).hasClass("easyui-combobox"))
			{
				$(inputItem).combobox("setValue", value);
			}
			else if (type == 'text' || type == 'textarea') {
				$(inputItem).val(value);
			} else if (type == 'select-one') {
				if($(inputItem).hasClass("xzqh"))
				{
					$(inputItem).setValue({value: value});
				}
				else
				{
					$(inputItem).val(value);
				}
			} else if (type == 'radio') {
				var name = $(inputItem).attr('name');
				$("#" + containerId + " input[name='" + name + "']")
						.each(function() {
							if (value == $(this).val()) {
								$(this).prop("checked", true);
							}
						})
			}
		}
	}
}

/**
 * 清空指定容器className控件值
 * 
 * @param {}
 *            containerId
 * @param {}
 *            className
 */
function clearContainerByClassName(containerId, className, ignoreIds) {
	var ary = $("#" + containerId + " ." + className);
	if (ary != null && ary.length != 0) {
		for (var i = 0; i < ary.length; i++) {
			var inputItem = ary[i];
			var id = $(inputItem).attr("id");
			var isIgnore = false;
			if (ignoreIds != null && ignoreIds.length != 0) {
				for (var i = 0; i < ignoreIds.length; i++) {
					var iid = ignoreIds[i];
					if (iid == id) {
						isIgnore = true;
						break;
					}
				}
			}
			if (!isIgnore) {
				var type = $(inputItem)[0].type;
				if (type == 'text' || type == 'password' || type == 'textarea') {
					$(inputItem).val("");
				} else if (type == 'select-one') {
					$(inputItem).val('');
				} else if (type == 'radio') {
					var name = $(inputItem).attr('name');
					$("#" + containerId + " input[name='" + name + "']").prop(
							'checked', false);
				}
			}
		}
	}
}

/**
 * 设置容器是否只读
 * 
 * @param {}
 *            reayOnly
 */
function setContainerIsReadOnly(containerId, isReadOnly, ignoreClass) {
	$("#" + containerId + " input ,select, textarea").each(function() {
		var class1 = $(this).attr("class");
		var isIgnore = false;
		if (!StringUtils.isEmpty(ignoreClass) && !StringUtils.isEmpty(class1)
				&& class1.indexOf(ignoreClass) != -1) {
			isIgnore = true;
		}
		var type = $(this)[0].type;
		if (!isIgnore)
		{
			if(isReadOnly)
			{
				if(type == 'radio')
				{
					 $(this).prop("disabled",true);  
				}
				else if (type == 'select-one') {
					$(this).prop('disabled', true);
				}
				else
				{
					$(this).attr("readonly", true);
				}
			}
			else
			{
				if(type == 'radio')
				{
					 $(this).attr("disabled",false);  
				}
				else if (type == 'select-one') {
					$(this).prop('disabled', false);
				}
				else
				{
					$(this).removeAttr("readonly");
				}
			}
		} 
	})
}

/**
 * 设置控件非空*号 容器ID为空 则整个页面符合className的控件都会添加
 * 
 * @param {}
 *            className
 * @param {}
 *            containerId
 */
function setNotNullControls(className, containerId) {
	if (StringUtils.isEmpty(containerId)) {
		$("." + className).each(function() {
			$(this)
					.after($('<span style="color: red; margin-left: 2px;">*</span>'));
		})
	} else {
		$("#" + containerId + " ." + className).each(function() {
			$(this)
					.after($('<span style="color: red; margin-left: 2px;">*</span>'));
		})
	}
}

/** 验证容器的数据 */
function validateContainerData(containerId) {
	var ary = $('#' + containerId + ' [validable="true"]');
	for (var i = 0; i < ary.length; i++) {
		var item = $(ary[i]);
		/**自定义验证函数*/
		var validateFunc = item.attr('validateFunc');
		validateFunc = eval(validateFunc);
		if(jQuery.isFunction(validateFunc))
		{
			var result = validateFunc.apply(this, item);
			if(!result) return false;
		}
		else
		{
			/** 标签 */
			var label = item.attr('label');
			/** 是否必输项 */
			var isrequired = item.attr('isRequired');
			/** 是否是全数字 */
			var isNum = item.attr('isNum');
			/** 是否全是正整数 */
			var isZzNum = item.attr('isZzNum');
			/** 字段长度 */
			var length = item.attr('length');
			var value = item.val();
			if (isrequired == "true") {
				if (StringUtils.isEmpty(value)) {
					var msg = label + '不能为空';
					validateLayerAlert(msg, item);
					return false;
				}
			}
			if (isNum) {
				if (!StringUtils.isEmpty(value) && isNaN(value)) {
					var msg = label + '只能含有数字';
					validateLayerAlert(msg, item);
					return false;
				}
			}
			if (isZzNum) {
				if (!StringUtils.isEmpty(value) && isZzNumCheck(value)) {
					var msg = label + '只能含正整数';
					validateLayerAlert(msg, item);
					return false;
				}
			}
			var len = StringUtils.getRealLength(value);
			if (len > length) {
				var msg = label + '最大长度为' + length;
				validateLayerAlert(msg, item);
				return false;
			}
		}
	}
	return true;
}


function isZzNumCheck(value){
	var flag = false;
	var number = /^(0|[1-9]\d*)$/;
	if(!number.test(value)){
		flag = true;
	}
	
	return flag;
}


function validateAlert(msg, item) {
	$.messager.alert('系统提示', msg, 'info', function() {
		item.focus();
	});
	
}

function validateLayerAlert(msg, item) {
	parent.layer.alert(msg, {
	    btn: ['确定'], //按钮
	    icon: 5, title:'系统提示'
	}, function(index){
		parent.layer.close(index);
		if(item)
		{
			item.focus();
		}
	});
}

function ajaxJsonResultAlert(msg, callback)
{
	parent.layer.alert(msg, {
	    icon: 1, title:'系统提示',closeBtn:0
	}, function(index){
		parent.layer.close(index);
		if(callback)
		{
			callback.apply(this);
		}
	});
}

function objectValueIsNullFormat(obj){
	for(var key in obj){
		var value = obj[key];
		if(StringUtils.isEmpty(value)){
			value = "";
			obj[key] = value;
		}
	}
	return obj;
}



