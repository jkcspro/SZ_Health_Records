/**
 * Created by Administrator on 2017/6/13.
 */
var gulp = require('gulp');
var clean = require('gulp-clean');
var rev = require('gulp-rev');
var cssmin = require('gulp-minify-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var revCollector = require('gulp-rev-collector');
var runSequence = require('run-sequence'); //执行顺序，避免
var fs = require('fs');
var path = require('path');
var postcss = require('gulp-postcss');
var connect = require('gulp-connect');
var data = require('gulp-data');
var ejs = require('gulp-ejs');

/*发布版本*/
var release_version = '';
/*配置文件的地方*/
var cfg_local = '';
/*模块路径配置*/
var module_path = {
  jkda_dir: __dirname + "/jkda" + "/" + release_version,
  myorder_dir: __dirname + "/yygh" + "/" + release_version,
  qyfw_dir: __dirname + "/qyfw" + "/" + release_version
}
/*发布的模块配置*/
var releases = [{
  module: 'gzd',
  version: release_version
}];
/*编译的模块配置*/
var modules = [{
  module: 'yygh',
  version: release_version
}, {
  module: 'jkda',
  version: release_version
}, {
  module: 'qyfw',
  version: release_version
}];
/*编译的资源配置*/
var statics = [{
    module: 'static',
    version: release_version
  },
  {
    module: 'quickJs',
    version: release_version
  },
];


/*静态资源*/
var dir_static = '/static';
/*模块页面模板路径 /src/templates*/
var dir_module_src = '';
/*模块配置文件路径*/
var dir_module_cfg = '/src/config';

/*编译根目录*/
var dir_dist = './dist';
var dir_dist_html = dir_dist + '/html';
var dir_dist_html_gzd = dir_dist_html + '/gzd';
var dir_dist_html_ysd = dir_dist_html + '/ysd';
var dir_dist_static = dir_dist + '/static';

var base = __dirname;

//监控任务
gulp.task('mointor', ['ejs', 'ejs-watch'], function () {
  var ary = releases.concat(modules).concat(statics);
  ary.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    var dir_src = './' + module + '/' + version;
    gulp.watch(dir_src).on('change', connect.reload);
  });
});
//编译html
gulp.task('ejs', function () {
  releases.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    var dir_src = './' + module + '/' + version + '/**/*.html';
    var cfg_final = getConfig();
    Object.assign(cfg_final, module_path)
    gulp.src(dir_src)
      .pipe(data(function (file) {
        var filePath = file.path;
        console.log(filePath);
        return cfg_final;
      }))
      .pipe(ejs().on('error', function (err) {
        console.log(err);
      }))
      .pipe(gulp.dest(dir_dist_html + "/" + module));
  });
});

gulp.task('ejs-watch', function () {
  modules.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    // var dir_src = './' + module + '/' + version + dir_module_src;
    var dir_src = './' + module + '/' + version + '/**/*';
    gulp.watch(dir_src + '/**', ['ejs']);
  });
});

//编译static
gulp.task('static', function () {
  statics.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    var dir_dist_mod = dir_dist + '/' + module;
    var dir_src_mod = "./" + module + '/' + version + '/**';
    return gulp.src(dir_src_mod)
      .pipe(gulp.dest(dir_dist_mod));
  });
});
// //编译quickJs
// gulp.task('quickJs', function () {
//   statics.forEach(function (item, index) {
//     var module = item['module'];
//     var version = item['version'];
//     var dir_dist_mod = dir_dist + '/' + module;
//     var dir_src_mod = "./" + module + '/' + version + '/**';
//     return gulp.src(dir_src_mod)
//       .pipe(gulp.dest(dir_dist_mod));
//   });
// });

function getConfig() {
  var cfg = {};
  var filePath = path.resolve('./config/' + cfg_local);
  var files = fs.readdirSync(filePath);
  files.forEach(function (filename) {
    if (endWith(filename, '.json')) {
      var json = JSON.parse(fs.readFileSync(filePath + '/' + filename));
      Object.assign(cfg, json);
    }
  });
  return cfg;
}

function endWith(str, endStr) {
  var d = str.length - endStr.length;
  return (d >= 0 && str.lastIndexOf(endStr) == d)
}


gulp.task('clean-all', function () {
  return gulp.src(dir_dist, {
      read: false
    })
    .pipe(clean());
});



gulp.task('build', function () {
  runSequence(
    'clean-all',
    'ejs',
    'min-css',
    'min-js',
    'build-lib',
    'build-images',
    'build-font',
    'rev',
    'build-edit',

  );
});

gulp.task('autoDev', function () {
  releases.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    // var dir_src = './' + module + '/' + version + '/**/*';
    var dir_src = './' + module + '/' + version;
    gulp.watch(dir_src + '/**', ['ejs']);
  })
  statics.forEach(function (item, index) {
    var module = item['module'];
    var version = item['version'];
    var dir_dist_mod = dir_dist + '/' + module;
    var dir_src_mod = "./" + module + '/' + version + '/**';
    gulp.watch(dir_src_mod + '/**', ['static']);
  })

});


gulp.task('local', function () {
  runSequence(
    'clean-all',
    'ejs',
    'static',
    'autoDev',
    'ejs-watch',
  );
});